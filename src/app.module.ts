import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Promocode } from './promocode/infrastructure/outcoming/storage/promocode.model';
import { PromocodeModule } from './promocode/promocode.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    TypeOrmModule.forRootAsync({
      useFactory: () => ({
        type: process.env.DB_TYPE as any,
        database: process.env.DB_HOST,
        entities: [Promocode],
        synchronize: process.env.DB_TYPEORM_SYNC === 'true',
      }),
    }),
    PromocodeModule,
  ],
  providers: [],
})
export class AppModule {}
