export type Anchor = string;
export type DataPath = string[];

export const DSL_FIELD_LOCATION = 'restrictions';
export const DSL_ANCHOR_PREFIX = '@';
export enum DslAnchors {
  DATE = '@date',
  AGE = '@age',
  METEO = '@meteo',
}
export enum DslLogicalAnchors {
  OR = '@or',
  AND = '@and',
}
export enum DslPrefixLogicalAnchors {
  NOT = '@not',
  DOUBLE_BANG = '@doubleBang',
}
export type Translation = string;
export type SpecificTypes = string;
export const DslLogicalAnchorMap = new Map<Anchor, Translation>();
DslLogicalAnchorMap.set(DslLogicalAnchors.OR, '||');
DslLogicalAnchorMap.set(DslLogicalAnchors.AND, '&&');
DslLogicalAnchorMap.set(DslPrefixLogicalAnchors.NOT, '!');
DslLogicalAnchorMap.set(DslPrefixLogicalAnchors.DOUBLE_BANG, '!!');

const rosettaMap = new Map<Anchor, DataPath>();
rosettaMap.set(DslAnchors.DATE, ['date']);
rosettaMap.set(DslAnchors.AGE, ['age']);
rosettaMap.set(`${DslAnchors.METEO}.is`, ['meteo', 'is']);
rosettaMap.set(`${DslAnchors.METEO}.temp`, ['meteo', 'temp']);

export type OperatorPatternMap = {
  [key: string]: [Translation, SpecificTypes?];
};
const OperatorPatternMap: {
  [key: string]: [Translation, SpecificTypes?];
} = {
  eq: ['===', ''],
  lt: ['<=', ''],
  gt: ['>=', ''],
  after: ['>=', 'date'],
  before: ['<=', 'date'],
};

export { rosettaMap, OperatorPatternMap };
