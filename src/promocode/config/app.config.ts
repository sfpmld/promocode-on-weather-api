import { registerAs } from '@nestjs/config';

export default registerAs('promocode-app', () => ({
  PORT: process.env.PORT || 3001,
  ENV: process.env.NODE_ENV || 'development',
  database: {
    TYPE: process.env.DB_TYPE || 'sqlite',
    HOST: process.env.DB_HOST || 'promocode.db',
    DB_TYPEORM_SYNC: process.env.DB_TYPEORM_SYNC || true,
  },
  api: {
    OPEN_WEATHER_API_URL: process.env.OPEN_WEATHER_API_URL,
    OPEN_WEATHER_API_KEY: process.env.OPEN_WEATHER_API_KEY,
  },
}));
