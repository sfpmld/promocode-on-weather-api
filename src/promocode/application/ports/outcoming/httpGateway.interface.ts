import { OpenWeatherResponse } from 'src/promocode/business/dtos/dataMapper';

export abstract class IHttpWeatherGateway {
  abstract getWeatherByTown(town: string): Promise<OpenWeatherResponse>;
}
