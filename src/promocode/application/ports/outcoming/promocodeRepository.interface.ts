import { PromocodeProps } from 'src/promocode/business/entities/promocode';

export abstract class IPromocodeRepository {
  abstract retrievePromocodeByName(
    promocodeName: string,
  ): Promise<PromocodeProps>;

  abstract storePromocode(promocode: PromocodeProps): Promise<boolean>;
}
