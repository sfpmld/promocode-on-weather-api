import { IUseCase } from '../../../shared/core/usecase.interfaces';
import { GetPromotionRequestDto } from '../../../business/contracts/input/getPromotionRequest';
import { GetPromotionResponseDto } from '../../../business/contracts/output/GetPromotionResponse';

export abstract class IGetPromotionUseCase extends IUseCase<
  GetPromotionRequestDto,
  GetPromotionResponseDto
> {
  abstract handle(
    request: GetPromotionRequestDto,
  ): Promise<GetPromotionResponseDto>;
}
