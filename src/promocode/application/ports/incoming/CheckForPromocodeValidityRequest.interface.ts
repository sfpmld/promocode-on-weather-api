import { PromocodeProps } from 'src/promocode/business/entities/promocode';
import { IsValidPromocodeRequestDto } from 'src/promocode/business/contracts/input/isValidPromocodeRequest';
import { IsValidPromocodeResponseDto } from 'src/promocode/business/contracts/output/IsValidPromocodeResponseDto';

export abstract class ICheckForPromocodeValidityUseCase {
  abstract handle(
    request: IsValidPromocodeRequestDto,
    promocode: PromocodeProps,
  ): Promise<IsValidPromocodeResponseDto>;
}
