import { AddPromocodeResponseDto } from 'src/promocode/business/contracts/output/AddPromocodeResponseDto';
import { AddPromocodeRequestDto } from '../../../business/contracts/input/AddPromocodeRequest';

export abstract class IAddPromocodeUseCase {
  abstract handle(
    request: AddPromocodeRequestDto,
  ): Promise<AddPromocodeResponseDto>;
}
