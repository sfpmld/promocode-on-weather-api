import { IDateProvider } from '..';

export class DateProviderSpy implements IDateProvider {
  protected _dateSpy = '2019-01-01';
  protected _getNowHaveBeenCalled = false;

  setup(date: string) {
    this._dateSpy = date;
    return this;
  }

  get getNowHaveBeenCalled() {
    return this._getNowHaveBeenCalled;
  }
  getNow() {
    this._getNowHaveBeenCalled = true;
    return this._dateSpy;
  }
}
