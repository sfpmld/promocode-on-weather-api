import { DateTime } from 'luxon';

export abstract class IDateProvider {
  abstract getNow(): string;
}

export class DateProvider implements IDateProvider {
  getNow(): string {
    return DateTime.now().toISODate();
  }
}
