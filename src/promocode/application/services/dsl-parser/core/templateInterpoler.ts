import { IsValidPromocodeRequestDto } from 'src/promocode/business/contracts/input/isValidPromocodeRequest';
import { DslAnchors, rosettaMap } from '../../../../config/rosettaStone';
import { getDataFromFieldsPath } from '../../../../shared/object';
import { IFormatter } from './stringFormatter';

export abstract class ITemplateInterpoler {
  abstract interpolate(template: string, data: any): string;
}

export class TemplateInterpoler implements ITemplateInterpoler {
  constructor(private formatter: IFormatter) {}

  public interpolate(template: string, data: IsValidPromocodeRequestDto) {
    let result = template;

    rosettaMap.forEach((dataPath, key) => {
      const regex = new RegExp(key, 'g');
      const matches = template.match(regex) || [];

      matches.forEach((anchor) => {
        const value = getDataFromFieldsPath(dataPath, data);
        if (value) {
          result = this._interpolate(result, anchor, value);
        }
      });
    });

    return result;
  }

  private _interpolate(
    template: string,
    anchor: string,
    value: IsValidPromocodeRequestDto,
  ) {
    const regex = new RegExp(anchor, 'g');
    const isDate = anchor === DslAnchors.DATE ? 'date' : undefined;
    const formattedPayloadString = this.formatter.format(value, isDate);

    return template.replace(regex, formattedPayloadString);
  }
}
