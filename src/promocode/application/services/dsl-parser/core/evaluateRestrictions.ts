export interface RestrictionEvaluation {
  result: boolean;
  errors: string[];
}

export abstract class IRestrictionEvaluator {
  abstract evaluate(interpolatedRestrictions: string): RestrictionEvaluation;
}

export class RestrictionEvaluator implements IRestrictionEvaluator {
  private filter: string;

  public evaluate(interpolatedRestrictions: string): RestrictionEvaluation {
    this.filter = this._makeFilterFromInterpolatedRestrictions(
      interpolatedRestrictions,
    );

    let capturedErrors: any[] = [];
    const captureErrors = (value: any) => {
      capturedErrors.push(value);
    };

    const fn = new Function(
      'capturedErrors',
      'captureErrors',
      `${this.filter}`,
    );

    const result: boolean = fn(capturedErrors, captureErrors);

    // When true obtained from OR sentences, previous errors are not relevant anymore
    if (result === true) capturedErrors = [];

    return { result, errors: capturedErrors };
  }

  private _makeFilterFromInterpolatedRestrictions(restrictions: string) {
    return `if(${restrictions}) return true; else return false;`;
  }
}
