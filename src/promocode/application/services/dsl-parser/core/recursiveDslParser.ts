import {
  DslAnchors,
  DslLogicalAnchorMap,
  DslLogicalAnchors,
  DslPrefixLogicalAnchors,
  DSL_ANCHOR_PREFIX,
  DSL_FIELD_LOCATION,
  OperatorPatternMap,
} from '../../../../config/rosettaStone';
import {
  isValueEnumKey,
  retrieveEnumKeyFromValue,
} from '../../../../shared/enum';
import { isEmptyObject, isStriclyObject } from '../../../../shared/object';
import { IFormatter } from './stringFormatter';

export abstract class IRecursiveDSLParser {
  abstract parseDSL(payload: any): string;
}

export class RecursiveDSLParser implements IRecursiveDSLParser {
  constructor(private stringFormatter: IFormatter) {}

  public static create(stringFormatter: IFormatter) {
    return new RecursiveDSLParser(stringFormatter);
  }

  public parseDSL(payload: any): string {
    return this.parseRestrictionBlock(payload);
  }

  private parseRestrictionBlock(payload: any): string {
    let restrictions = {};
    if (!payload) return '';
    if (payload.hasOwnProperty(DSL_FIELD_LOCATION)) {
      restrictions = payload[DSL_FIELD_LOCATION];
    }

    const conditions = this.parseRestriction(restrictions, '');

    return conditions;
  }

  private getTranslatedOperatorFor(anchor: DslLogicalAnchors) {
    return `${DslLogicalAnchorMap.get(anchor)} `;
  }

  private parseRestriction(
    restrictions: any,
    operator: string,
    prefixOp?: string,
  ): string {
    let restriction = '';
    let op = '';

    if (isEmptyObject(restrictions)) return '';
    op =
      operator !== ''
        ? operator
        : this.getTranslatedOperatorFor(DslLogicalAnchors.AND);

    const target = restrictions[0];

    for (const key in target) {
      if (isValueEnumKey(key, DslAnchors)) {
        restriction += this.translateRestrictionBlock(
          key,
          target[key],
          prefixOp,
        );
        restriction += this.connectRestrictionBlockWhenNeeded(
          restrictions,
          operator !== '' ? operator : op,
        );
      }

      if (isValueEnumKey(key, DslLogicalAnchors)) {
        op = this.getTranslatedOperatorFor(
          DslLogicalAnchors[retrieveEnumKeyFromValue(key, DslLogicalAnchors)],
        );
        restriction += this.parseSubRestrictionBlock(target[key], op);
        restriction += this.connectRestrictionBlockWhenNeeded(
          restrictions,
          operator !== '' ? operator : op,
        );
      }

      if (isValueEnumKey(key, DslPrefixLogicalAnchors)) {
        const prefixOperator = this.getTranslatedOperatorFor(
          DslPrefixLogicalAnchors[
            retrieveEnumKeyFromValue(key, DslPrefixLogicalAnchors)
          ],
        );
        restriction += this.parseSubRestrictionBlock(
          target[key],
          op,
          prefixOperator,
        );
        restriction += this.connectRestrictionBlockWhenNeeded(
          restrictions,
          operator !== '' ? operator : op,
        );
      }
    }
    restriction += this.parseRestriction(restrictions.slice(1), op, prefixOp);

    return restriction;
  }

  private parseSubRestrictionBlock(
    block: any,
    operator: string,
    prefixOp?: string,
  ) {
    return `(${this.parseRestriction(block, operator, prefixOp)})`;
  }

  private translateRestrictionBlock(
    key: string,
    block: any,
    prefixOp = '',
  ): string {
    return `(${prefixOp}${this.parseRestrictionsKeys(
      key,
      block,
    )} || captureErrors('Invalid ${this.formatAnchorViewName(
      key,
    )}: required => ${Object.entries(block)}')) `;
  }

  private connectRestrictionBlockWhenNeeded(block: any, operator: string) {
    return block.length > 1 ? ` ${operator}` : '';
  }

  private parseRestrictionsKeys(fieldName: string, obj: object): string {
    if (isEmptyObject(obj)) return '';
    const defaultConnector = `${DslLogicalAnchorMap.get(
      DslLogicalAnchors.AND,
    )}`;

    const response = Object.entries(obj).reduce((acc, [key, value]) => {
      if (key in OperatorPatternMap) {
        const restrictionsRule = this.translateRestrictionRule(
          fieldName,
          key,
          value,
        );
        acc +=
          acc === ''
            ? restrictionsRule
            : this.appendValue(restrictionsRule, defaultConnector);
      } else {
        if (isStriclyObject(value)) {
          const flattenedObject = this.flattenAndParseDeepObjectRestriction(
            fieldName,
            key,
            value,
          );
          acc +=
            acc === ''
              ? flattenedObject
              : this.appendBlock(flattenedObject, defaultConnector);
        } else {
          const flattenedRestriction =
            this.flattenAndTranslateRestrictionObject(fieldName, key, value);
          acc +=
            acc === ''
              ? flattenedRestriction
              : this.appendValue(flattenedRestriction, defaultConnector);
        }
      }
      return acc;
    }, '');

    return response;
  }

  private appendValue(value: any, operator: string): string {
    return `${operator} ${value} `;
  }

  private appendBlock(block: any, operator: string): string {
    return `${operator} (${block})`;
  }

  private translateRestrictionRule(
    fieldName: string,
    key: string,
    value: any,
  ): string {
    const translatedOperator = OperatorPatternMap[key][0];
    const hasSpecificType = OperatorPatternMap[key][1];

    return `${fieldName} ${translatedOperator} ${this.stringFormatter.format(
      value,
      hasSpecificType,
    )} `;
  }

  private flattenAndParseDeepObjectRestriction(
    fieldName: string,
    key: string,
    value: any,
  ): string {
    return this.parseRestrictionsKeys(`${fieldName}.${key}`, value);
  }

  private flattenAndTranslateRestrictionObject(
    fieldName: string,
    key: string,
    value: any,
  ): string {
    return `${fieldName}.${key} === ${this.stringFormatter.format(value)} `;
  }

  private formatAnchorViewName(anchor: string): string {
    return anchor.replace(DSL_ANCHOR_PREFIX, '');
  }
}
