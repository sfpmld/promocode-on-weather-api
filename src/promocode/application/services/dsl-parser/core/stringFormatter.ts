export abstract class IFormatter {
  abstract format(value: any, specialType?: string): string;
}

export class Formatter implements IFormatter {
  format(value, specialType?) {
    if (specialType === 'date') {
      return `new Date('${value}')`;
    }
    if (typeof value === 'string') {
      return `'${value}'`;
    }

    return value;
  }
}
