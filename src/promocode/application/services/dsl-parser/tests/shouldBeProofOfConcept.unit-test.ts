import { IsValidPromocodeRequestDto } from '../../../../business/contracts/input/isValidPromocodeRequest';
import {
  RestrictionEvaluation,
  RestrictionEvaluator,
} from '../core/evaluateRestrictions';
import { setupInterpolatedRestrictionsFixtures } from '../../../../../../testSettings/fixtures/makeInterpolatedRestrictions';

// Mocks
class SUT {
  private sut: RestrictionEvaluator;
  private testing: typeof RestrictionEvaluator;
  public result: RestrictionEvaluation;
  constructor(testing: typeof RestrictionEvaluator) {
    this.testing = testing;
  }

  public setupWith() {
    this.sut = new this.testing();
    return this;
  }

  public processToRestrictionEvaluation(interpolatedRestrictions: string) {
    this.result = this.sut.evaluate(interpolatedRestrictions);
  }

  public shouldHaveEvaluatedRestrictions(
    expectedResult: boolean,
    eventualErrors?: string[],
  ) {
    const { result, errors } = this.result;
    expect(result).toBe(expectedResult);
    expect(errors).toEqual(expect.arrayContaining(eventualErrors));
  }
}

let sut;
beforeEach(async () => {
  sut = new SUT(RestrictionEvaluator).setupWith();
});

describe('Restriction Parsing >', () => {
  describe(`Given parsed restrictions string with [INVALID date conditions]`, () => {
    describe(`When process by template interpolating function`, () => {
      it('Then it should return false and an array of related errors', async () => {
        // Arrange
        const expectedErrors = ['Invalid date'];
        const realInvalidDataToInterpolate: IsValidPromocodeRequestDto = {
          date: '2016-01-01',
          age: 16,
          meteo: {
            is: 'rainy',
            temp: 15,
          },
        };
        const interpolatedRestrictions = setupInterpolatedRestrictionsFixtures(
          realInvalidDataToInterpolate,
        );

        // Act
        sut.processToRestrictionEvaluation(interpolatedRestrictions);

        // Assert
        sut.shouldHaveEvaluatedRestrictions(false, expectedErrors);
      });
    });
  });

  describe(`Given parsed restrictions string with [INVALID age | meteo conditions]`, () => {
    describe(`When process by template interpolating function`, () => {
      it('Then it should return false and an array of related errors', async () => {
        // Arrange
        const expectedErrors = ['Invalid age', 'Invalid meteo'];
        const realInvalidDataToInterpolate: IsValidPromocodeRequestDto = {
          date: '2019-01-01',
          age: 16,
          meteo: {
            is: 'rainy',
            temp: 15,
          },
        };
        const interpolatedRestrictions = setupInterpolatedRestrictionsFixtures(
          realInvalidDataToInterpolate,
        );

        // Act
        sut.processToRestrictionEvaluation(interpolatedRestrictions);

        // Assert
        sut.shouldHaveEvaluatedRestrictions(false, expectedErrors);
      });
    });
  });

  describe(`Given parsed restrictions string with [INVALID age conditions]`, () => {
    describe(`When process by template interpolating function`, () => {
      it('Then it should return false and an array of related errors', async () => {
        // Arrange
        const expectedErrors = ['Invalid age'];
        const realInvalidDataToInterpolate: IsValidPromocodeRequestDto = {
          date: '2019-01-01',
          age: 36,
          meteo: {
            is: 'clear',
            temp: 15,
          },
        };
        const interpolatedRestrictions = setupInterpolatedRestrictionsFixtures(
          realInvalidDataToInterpolate,
        );

        // Act
        sut.processToRestrictionEvaluation(interpolatedRestrictions);

        // Assert
        sut.shouldHaveEvaluatedRestrictions(false, expectedErrors);
      });
    });
  });

  describe(`Given parsed restrictions string with [VALID conditions]`, () => {
    describe(`When process by template interpolating function`, () => {
      it('Then it should return false and an array of related errors', async () => {
        // Arrange
        const expectedErrors = [];
        const realValidDataToInterpolate: IsValidPromocodeRequestDto = {
          date: '2019-01-01',
          age: 16,
          meteo: {
            is: 'clear',
            temp: 15,
          },
        };
        const interpolatedRestrictions = setupInterpolatedRestrictionsFixtures(
          realValidDataToInterpolate,
        );

        // Act
        sut.processToRestrictionEvaluation(interpolatedRestrictions);

        // Assert
        sut.shouldHaveEvaluatedRestrictions(true, expectedErrors);
      });
    });
  });
});
