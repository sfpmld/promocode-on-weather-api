import { RecursiveDSLParser } from '../core/recursiveDslParser';
import * as samplePayload from '../../../../../../testSettings/fixtures/promocode-sample.json';
import { predicateRegex } from '../../../../shared/regex/general';
import { Formatter, IFormatter } from '../core/stringFormatter';

class SUT {
  private sut: typeof RecursiveDSLParser;
  private testing: RecursiveDSLParser;
  public restrictions: string;

  constructor(testing: typeof RecursiveDSLParser) {
    this.sut = testing;
  }

  setupWith(formatter: IFormatter) {
    this.testing = this.sut.create(formatter);
    return this;
  }

  public processDSLFromPayload(payload) {
    this.restrictions = this.testing.parseDSL(payload);
  }

  public shouldParseRestriction() {
    expect(this.restrictions).toBeDefined();
  }

  public shouldParseRestrictionInStringFormat() {
    expect(this.restrictions).toEqual(expect.any(String));
  }

  public shouldTranslateRestrictionToPredicate(regexp: RegExp) {
    expect(this.restrictions).toEqual(expect.stringMatching(regexp));
  }

  public shouldTranslateBetweenDateRestrictionToPredicateWith(before, after) {
    expect(this.restrictions).toEqual(
      expect.stringMatching(`date <= .*${before}.*`),
    );
    expect(this.restrictions).toEqual(
      expect.stringMatching(`date >= .*${after}.*`),
    );
  }

  public shouldTranslateLogicalOperatorToPredicate(
    operator: '||' | '&&' | '!',
  ) {
    expect(this.restrictions).toEqual(expect.stringMatching(operator));
  }

  public shouldTranslateAgeRestrictionToPredicate() {
    expect(this.restrictions).toEqual(expect.stringMatching(/age === 40/g));
  }

  public shouldTranslateAgeRangeRestrictionToPredicate() {
    expect(this.restrictions).toEqual(expect.stringMatching(/age >= 15/g));
    expect(this.restrictions).toEqual(expect.stringMatching(/age <= 30/g));
  }
}

let sut: SUT;
beforeEach(async () => {
  sut = new SUT(RecursiveDSLParser).setupWith(new Formatter());
});

describe('Restriction Parsing >', () => {
  describe(`Given incoming payload containing a restriction DSL sample`, () => {
    describe(`When processed by DSL parser`, () => {
      it(`Then it should be able to detect restriction block`, async () => {
        sut.processDSLFromPayload(samplePayload);

        sut.shouldParseRestriction();
      });

      it('And it should translate the restriction translation in a string format', async () => {
        sut.processDSLFromPayload(samplePayload);

        sut.shouldParseRestrictionInStringFormat();
      });

      it('Then parsed restriction should correspond to a query-like string', async () => {
        sut.processDSLFromPayload(samplePayload);

        sut.shouldTranslateRestrictionToPredicate(predicateRegex);
      });

      it('Then gt, lt, eq of internal DSL node should be translated in logical form', async () => {
        const simpleBlockSample = {
          restrictions: [
            {
              '@age': {
                gt: 15,
                lt: 30,
              },
            },
          ],
        };

        sut.processDSLFromPayload(simpleBlockSample);

        sut.shouldTranslateRestrictionToPredicate(/age >= 15/g);
        sut.shouldTranslateRestrictionToPredicate(/age <= 30/g);
      });

      it('Then nested operator in objects should recursively be translated also', async () => {
        const nestedBlockSample = {
          restrictions: [
            {
              '@meteo': {
                is: 'rain',
                temp: {
                  gt: 15,
                  lt: 30,
                },
              },
            },
          ],
        };

        sut.processDSLFromPayload(nestedBlockSample);

        sut.shouldTranslateRestrictionToPredicate(/meteo.temp >= 15/g);
        sut.shouldTranslateRestrictionToPredicate(/meteo.temp <= 30/g);
      });

      it('And @date anchor with "after" and "before" should be translated as "date <= after" and date >= before', async () => {
        // Arrange
        const before = samplePayload.restrictions[0]['@date'].before;
        const after = samplePayload.restrictions[0]['@date'].after;

        sut.processDSLFromPayload(samplePayload);

        sut.shouldTranslateBetweenDateRestrictionToPredicateWith(before, after);
      });

      it('And @or anchor should be treated as sub-restrictions array', async () => {
        sut.processDSLFromPayload(samplePayload);

        sut.shouldTranslateLogicalOperatorToPredicate('||');
      });

      it('And @and anchor should be treated as sub-restrictions array', async () => {
        sut.processDSLFromPayload(samplePayload);

        sut.shouldTranslateLogicalOperatorToPredicate('&&');
      });

      it('And @not anchor should be treated as sub-restrictions array', async () => {
        sut.processDSLFromPayload(samplePayload);

        sut.shouldTranslateLogicalOperatorToPredicate('!');
      });

      it('Then given @age from restriction payload sample field should be processed as well', async () => {
        sut.processDSLFromPayload(samplePayload);

        sut.shouldTranslateAgeRestrictionToPredicate();
      });

      it('Then @age range with lt and gt operator should be translated', async () => {
        sut.processDSLFromPayload(samplePayload);

        sut.shouldTranslateAgeRangeRestrictionToPredicate();
      });
    });

    describe('Snapshot of resulting sample restriction BEFORE interpolation >', () => {
      it('Then it should match snapshot', async () => {
        sut.processDSLFromPayload(samplePayload);

        expect(sut.restrictions).toMatchInlineSnapshot(
          `"(@date >= new Date('2019-01-01') && @date <= new Date('2020-06-30')   || captureErrors('Invalid date: required => after,2019-01-01,before,2020-06-30'))  && (((@age === 40  || captureErrors('Invalid age: required => eq,40'))  && ((! @meteo.is === 'clear'  || captureErrors('Invalid meteo: required => is,clear')) )) || ((@age <= 30 && @age >= 15   || captureErrors('Invalid age: required => lt,30,gt,15'))  && (@meteo.is === 'clear' && (@meteo.temp >= 15 ) || captureErrors('Invalid meteo: required => is,clear,temp,[object Object]')) ))"`,
        );
      });
    });
  });
});
