import { TemplateInterpoler } from '../core/templateInterpoler';

import { Formatter, IFormatter } from '../core/stringFormatter';
import { parsedRestrictions } from '../../../../../../testSettings/fixtures/raw-parsed-restriction-template-string';
import { IsValidPromocodeRequestDto } from '../../../../business/contracts/input/isValidPromocodeRequest';

class SUT {
  private sut: typeof TemplateInterpoler;
  private testing: TemplateInterpoler;
  public result: string;
  constructor(sut: typeof TemplateInterpoler) {
    this.sut = sut;
  }

  public setupWith(stringFormatter: IFormatter) {
    this.testing = new this.sut(stringFormatter);
    return this;
  }

  public processTemplateInterpolation(template: string, data: any) {
    this.result = this.testing.interpolate(template, data);
  }

  public shouldInterpolateAnchorsWith(anchor: string, data: string) {
    expect(this.result).not.toMatch(anchor);
    expect(this.result).toMatch(data);
  }
}

let sut;
beforeEach(async () => {
  sut = new SUT(TemplateInterpoler).setupWith(new Formatter());
});
describe('Restriction Parsing >', () => {
  describe(`Given parsed restrictions string`, () => {
    describe(`When process by template interpolating function`, () => {
      it(`Then it should replace @date anchor by appropriate real object value`, async () => {
        const realDataToInterpolate: IsValidPromocodeRequestDto = {
          date: '2019-01-01',
        };

        sut.processTemplateInterpolation(
          parsedRestrictions,
          realDataToInterpolate,
        );

        sut.shouldInterpolateAnchorsWith('@date', realDataToInterpolate.date);
      });

      it(`Then it should replace @age anchor by appropriate real object value`, async () => {
        const realDataToInterpolate: IsValidPromocodeRequestDto = {
          date: '2019-01-01',
          age: 40,
        };

        sut.processTemplateInterpolation(
          parsedRestrictions,
          realDataToInterpolate,
        );

        sut.shouldInterpolateAnchorsWith(
          '@age',
          `${realDataToInterpolate.age}`,
        );
      });

      it(`Then it should replace @meteo anchor by appropriate real object value`, async () => {
        const realDataToInterpolate: IsValidPromocodeRequestDto = {
          date: '2019-01-01',
          age: 40,
          meteo: {
            is: 'clear',
          },
        };

        sut.processTemplateInterpolation(
          parsedRestrictions,
          realDataToInterpolate,
        );

        sut.shouldInterpolateAnchorsWith(
          '@meteo.is',
          `'${realDataToInterpolate.meteo.is}'`,
        );
      });

      it(`Then it should replace @meteo anchor by appropriate real object value`, async () => {
        const realDataToInterpolate: IsValidPromocodeRequestDto = {
          date: '2019-01-01',
          age: 40,
          meteo: {
            is: 'clear',
            temp: 15,
          },
        };

        sut.processTemplateInterpolation(
          parsedRestrictions,
          realDataToInterpolate,
        );

        sut.shouldInterpolateAnchorsWith(
          '@meteo.temp',
          `${realDataToInterpolate.meteo.temp}`,
        );
      });

      describe('Snapshot of resulting sample restriction AFTER interpolation', () => {
        it('Then it should match snapshot', async () => {
          const realDataToInterpolate: IsValidPromocodeRequestDto = {
            date: '2019-01-01',
            age: 40,
            meteo: {
              is: 'clear',
              temp: 15,
            },
          };

          sut.processTemplateInterpolation(
            parsedRestrictions,
            realDataToInterpolate,
          );

          expect(sut.result).toMatchInlineSnapshot(
            `"(new Date('2019-01-01') >= new Date('2019-01-01') && new Date('2019-01-01') <= new Date('2020-06-30')   || captureErrors('Invalid date'))  && ((40 === 40  || captureErrors('Invalid age'))  || ((40 <= 30 && 40 >= 15   || captureErrors('Invalid age'))  && ('clear' === 'clear' && (15 >= 15 ) || captureErrors('Invalid meteo')) ))"`,
          );
        });
      });
    });
  });
});
