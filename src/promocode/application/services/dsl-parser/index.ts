import { PromocodeProps } from 'src/promocode/business/entities/promocode';
import { IsValidPromocodeRequestDto } from 'src/promocode/business/contracts/input/isValidPromocodeRequest';
import {
  IRestrictionEvaluator,
  RestrictionEvaluation,
} from './core/evaluateRestrictions';
import { IRecursiveDSLParser } from './core/recursiveDslParser';
import { ITemplateInterpoler } from './core/templateInterpoler';

export abstract class ICheckPromocodeValidityService {
  abstract validate(
    request: IsValidPromocodeRequestDto,
    promocode: PromocodeProps,
  ): RestrictionEvaluation;
}

export class CheckPromocodeValidityService
  implements ICheckPromocodeValidityService
{
  constructor(
    private dslparser: IRecursiveDSLParser,
    private templateInterpoler: ITemplateInterpoler,
    private restrictionEvaluator: IRestrictionEvaluator,
  ) {}

  validate(
    request: IsValidPromocodeRequestDto,
    promocode: PromocodeProps,
  ): RestrictionEvaluation {
    const parsedRestrictions = this.dslparser.parseDSL(promocode);
    const interpolatedRestrictionsWithData =
      this.templateInterpoler.interpolate(parsedRestrictions, request);
    const restrictionEvaluation = this.restrictionEvaluator.evaluate(
      interpolatedRestrictionsWithData,
    );

    const { result, errors } = restrictionEvaluation;
    const filteredErrors = [...new Set(errors)];

    return {
      result,
      errors: filteredErrors,
    };
  }
}
