import { IsValidPromocodeRequestDto } from '../../../business/contracts/input/isValidPromocodeRequest';
import { IsValidPromocodeResponseDto } from '../../../business/contracts/output/IsValidPromocodeResponseDto';
import { RestrictionEvaluator } from '../../services/dsl-parser/core/evaluateRestrictions';
import { RecursiveDSLParser } from '../../services/dsl-parser/core/recursiveDslParser';
import { Formatter } from '../../services/dsl-parser/core/stringFormatter';
import { TemplateInterpoler } from '../../services/dsl-parser/core/templateInterpoler';
import { CheckForPromocodeValidityUseCase } from '../checkForPromocodeValidity.request';
import * as promocodeSample from '../../../../../testSettings/fixtures/promocode-sample.json';
import { PromocodeStatusEnum } from '../../../business/contracts/output/GetPromotionResponse';
import { CheckPromocodeValidityService } from '../../services/dsl-parser';

// Mocks
class SUT {
  private testing: typeof CheckForPromocodeValidityUseCase;
  private result: IsValidPromocodeResponseDto;
  private sut: CheckForPromocodeValidityUseCase;

  constructor(testing: typeof CheckForPromocodeValidityUseCase) {
    this.testing = testing;
  }

  setupWith(validationService: CheckPromocodeValidityService) {
    this.sut = new this.testing(validationService);
    return this;
  }

  async processForCheckingValidity(validRequest: IsValidPromocodeRequestDto) {
    this.result = await this.sut.handle(validRequest, promocodeSample);
  }

  shouldReturn(successResponse: IsValidPromocodeResponseDto) {
    expect(this.result).toEqual(
      expect.objectContaining({
        ...successResponse,
      }),
    );
  }
}

// Helpers
let sut: SUT;

beforeEach(async () => {
  sut = new SUT(CheckForPromocodeValidityUseCase).setupWith(
    new CheckPromocodeValidityService(
      new RecursiveDSLParser(new Formatter()),
      new TemplateInterpoler(new Formatter()),
      new RestrictionEvaluator(),
    ),
  );
});

describe(`Story: User sends VALID request for a given promocode`, () => {
  describe(`Rule: Service is able to check for promocode validation on the base of the date of the day, age of the user and the some weather informations`, () => {
    describe(`Given incoming valid param to check for promocode's validity request`, () => {
      describe(`When processed by CheckForValidity UseCase`, () => {
        it(`Then it should return an ACCEPTED response`, async () => {
          const validRequest = {
            date: '2019-01-01',
            age: 25,
            meteo: {
              is: 'clear',
              temp: 16,
            },
          };

          await sut.processForCheckingValidity(validRequest);

          sut.shouldReturn({
            promocode_name: promocodeSample.name,
            status: PromocodeStatusEnum.ACCEPTED,
            avantage: { percent: promocodeSample.avantage.percent },
          });
        });
      });
    });

    describe(`Given incoming INVALID param to check for promocode's validity request`, () => {
      describe(`When processed by CheckForValidity UseCase`, () => {
        it(`Then it should return a DENIED response`, async () => {
          const inValidRequest = {
            date: '2019-01-01',
            age: 35,
            meteo: {
              is: 'clear',
              temp: 16,
            },
          };

          await sut.processForCheckingValidity(inValidRequest);

          sut.shouldReturn({
            promocode_name: promocodeSample.name,
            status: PromocodeStatusEnum.DENIED,
            reasons: [
              'Invalid age: required => eq,40',
              'Invalid age: required => lt,30,gt,15',
            ],
          });
        });
      });
    });
  });
});
