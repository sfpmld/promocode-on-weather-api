import { GetPromotionRequestDto } from '../../../business/contracts/input/getPromotionRequest';
import { HttpWeatherGatewaySpy } from '../../../infrastructure/outcoming/http-gateways/open-weather-map/mocks/httpWeatherGateway.spy';

import { dbStorageSpy } from '../../../infrastructure/outcoming/storage/mocks/dbStorage.spy';
import { IHttpWeatherGateway } from '../../ports/outcoming/httpGateway.interface';
import { GetPromotionRequestUsecase } from '../getPromotion.request';
import { IDateProvider } from '../../services/date-provider';
import { DateProviderSpy } from '../../services/date-provider/mocks/dateProvider.spy';
import { ICheckForPromocodeValidityUseCase } from '../../ports/incoming/CheckForPromocodeValidityRequest.interface';
import { CheckForPromocodeValidityUseCase } from '../checkForPromocodeValidity.request';
import { RecursiveDSLParser } from '../../services/dsl-parser/core/recursiveDslParser';
import { TemplateInterpoler } from '../../services/dsl-parser/core/templateInterpoler';
import { RestrictionEvaluator } from '../../services/dsl-parser/core/evaluateRestrictions';
import { Formatter } from '../../services/dsl-parser/core/stringFormatter';
import { CheckPromocodeValidityService } from '../../services/dsl-parser';
import { PromocodeStatusEnum } from '../../../business/contracts/output/GetPromotionResponse';
import { IPromocodeRepository } from '../../ports/outcoming/promocodeRepository.interface';

class SUT {
  private testing: typeof GetPromotionRequestUsecase;
  private sut: GetPromotionRequestUsecase;
  private storageAdapter: IPromocodeRepository & dbStorageSpy;
  private httpWeatherGateway: IHttpWeatherGateway & HttpWeatherGatewaySpy;
  private result: any;

  constructor(testing: typeof GetPromotionRequestUsecase) {
    this.testing = testing;
  }

  setupWith(
    storageAdapter: IPromocodeRepository & dbStorageSpy,
    httpWeatherGateway: IHttpWeatherGateway & HttpWeatherGatewaySpy,
    dateProvider: IDateProvider & DateProviderSpy,
    validationUsecase: ICheckForPromocodeValidityUseCase,
  ) {
    this.sut = new this.testing(
      storageAdapter,
      httpWeatherGateway,
      dateProvider,
      validationUsecase,
    );
    this.storageAdapter = storageAdapter;
    this.httpWeatherGateway = httpWeatherGateway;
    return this;
  }

  async handleRequest(getPromotionRequest: GetPromotionRequestDto) {
    this.result = await this.sut.handle(getPromotionRequest);
  }

  shouldRetrievePromocodeByName(promocodeName: string) {
    expect(this.storageAdapter.retrievePromocodeByNameWasCalled).toBe(true);
    expect(this.storageAdapter.beenCalledWithPromocodeName).toBe(promocodeName);
  }

  shouldRetrieveActualMeteoData() {
    expect(this.httpWeatherGateway.getWeatherByTownHaveBeenCalled).toBe(true);
  }

  shouldValidatePromocodeWithUserRequestAndWeatherData() {
    expect(this.result).toEqual(
      expect.objectContaining({
        promocode_name: 'WeatherCode',
        status: PromocodeStatusEnum.ACCEPTED,
        avantage: { percent: 20 },
      }),
    );
  }

  shouldInvalidatePromocodeWithUserRequestAndWeatherData(error: string[]) {
    expect(this.result).toEqual(
      expect.objectContaining({
        promocode_name: 'WeatherCode',
        status: PromocodeStatusEnum.DENIED,
        reasons: error,
      }),
    );
  }
}

let sut: SUT;
beforeEach(async () => {
  sut = new SUT(GetPromotionRequestUsecase).setupWith(
    new dbStorageSpy(),
    new HttpWeatherGatewaySpy(),
    new DateProviderSpy(),
    new CheckForPromocodeValidityUseCase(
      new CheckPromocodeValidityService(
        new RecursiveDSLParser(new Formatter()),
        new TemplateInterpoler(new Formatter()),
        new RestrictionEvaluator(),
      ),
    ),
  );
});

describe(`Story: User sends valid request for a given promocode`, () => {
  describe(`Rule: valid promocode request must return an "accepted" and return the promo percentage`, () => {
    describe(`Given incoming VALID request for a promocode`, () => {
      const expectedPromocodeName = 'WeatherCode';
      const testTown = 'Paris';
      const getPromotionRequest: GetPromotionRequestDto = {
        promocode_name: expectedPromocodeName,
        arguments: {
          age: 25,
          meteo: {
            town: testTown,
          },
        },
      };

      describe(`When processed by isValid Promocode UseCase`, () => {
        it(`Then it should retrieve target promocode via its promocode's name`, async () => {
          // Act
          await sut.handleRequest(getPromotionRequest);

          // Assert
          sut.shouldRetrievePromocodeByName(expectedPromocodeName);
        });

        it('Then it should retrieve meteo informations for given town in request', async () => {
          // Act
          await sut.handleRequest(getPromotionRequest);

          // Assert
          sut.shouldRetrieveActualMeteoData();
        });

        it(`Then it should use user request data and retrieved weather data to check for promocode validation
           AND return a succes response with percentage avantage`, async () => {
          // Act
          await sut.handleRequest(getPromotionRequest);

          // Assert
          sut.shouldValidatePromocodeWithUserRequestAndWeatherData();
        });
      });
    });
  });
});

describe(`Story: User sends NON VALID request for a given promocode`, () => {
  describe(`Rule: non valid promocode request must return a "denied" and return the reason why data was not right`, () => {
    describe(`Given incoming NON VALID request for a promocode`, () => {
      const expectedPromocodeName = 'WeatherCode';
      const expectedError = [
        'Invalid age: required => eq,40',
        'Invalid age: required => lt,30,gt,15',
      ];
      const testTown = 'Paris';
      const getPromotionRequest: GetPromotionRequestDto = {
        promocode_name: expectedPromocodeName,
        arguments: {
          age: 65,
          meteo: {
            town: testTown,
          },
        },
      };

      it(`Then it should use user request data and retrieved weather data to check for promocode validation
           AND return a succes response with percentage avantage`, async () => {
        // Act
        await sut.handleRequest(getPromotionRequest);

        // Assert
        sut.shouldInvalidatePromocodeWithUserRequestAndWeatherData(
          expectedError,
        );
      });
    });
  });
});
