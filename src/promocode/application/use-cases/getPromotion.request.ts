import { DataMapper } from '../../business/dtos/dataMapper';
import { GetPromotionRequestDto } from '../../business/contracts/input/getPromotionRequest';
import { IsValidPromocodeRequestDto } from '../../business/contracts/input/isValidPromocodeRequest';
import { GetPromotionResponseDto } from '../../business/contracts/output/GetPromotionResponse';

import { IGetPromotionUseCase } from '../ports/incoming/GetPromotionRequest.interface';
import { IHttpWeatherGateway } from '../ports/outcoming/httpGateway.interface';
import { IDateProvider } from '../services/date-provider';
import { ICheckForPromocodeValidityUseCase } from '../ports/incoming/CheckForPromocodeValidityRequest.interface';
import { IPromocodeRepository } from '../ports/outcoming/promocodeRepository.interface';

export class GetPromotionRequestUsecase implements IGetPromotionUseCase {
  constructor(
    private storageAdapter: IPromocodeRepository,
    private httpWeatherGateway: IHttpWeatherGateway,
    private dateProvider: IDateProvider,
    private validationUsecase: ICheckForPromocodeValidityUseCase,
  ) {}

  async handle(
    request: GetPromotionRequestDto,
  ): Promise<GetPromotionResponseDto> {
    const {
      promocode_name: promocodeName,
      arguments: {
        age,
        meteo: { town },
      },
    } = request;
    const date = this.dateProvider.getNow();

    const promocode = await this.storageAdapter.retrievePromocodeByName(
      promocodeName,
    );

    if (!promocode) {
      return DataMapper.toViewFailure(promocodeName, ['Promocode not found']);
    }

    const actualMeteoData = await this.httpWeatherGateway.getWeatherByTown(
      town,
    );
    const formattedMeteoData = DataMapper.fromWeatherAPI(actualMeteoData);

    const isPromocodeValidInput: IsValidPromocodeRequestDto =
      DataMapper.toValidation(date, age, formattedMeteoData);

    const isPromocodeValid = await this.validationUsecase.handle(
      isPromocodeValidInput,
      promocode,
    );

    return isPromocodeValid;
  }
}
