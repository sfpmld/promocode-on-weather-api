import { Promocode, PromocodeProps } from '../../business/entities/promocode';
import { IsValidPromocodeRequestDto } from '../../business/contracts/input/isValidPromocodeRequest';
import { IsValidPromocodeResponseDto } from '../../business/contracts/output/IsValidPromocodeResponseDto';
import { ICheckForPromocodeValidityUseCase } from '../ports/incoming/CheckForPromocodeValidityRequest.interface';
import { ICheckPromocodeValidityService } from '../services/dsl-parser';

export class CheckForPromocodeValidityUseCase
  implements ICheckForPromocodeValidityUseCase
{
  constructor(private validationService: ICheckPromocodeValidityService) {}

  async handle(
    request: IsValidPromocodeRequestDto,
    promocode: PromocodeProps,
  ): Promise<IsValidPromocodeResponseDto> {
    const { result, errors } = this.validationService.validate(
      request,
      promocode,
    );

    const promocodeEntity = Promocode.create(
      promocode._id,
      promocode.name,
      promocode.avantage,
      promocode.restrictions,
    );

    if (!result) {
      return promocodeEntity.getDeniedPromocodeResponse(errors);
    }
    return promocodeEntity.getValidPromocodeResponse();
  }
}
