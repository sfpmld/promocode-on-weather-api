import { IHttpWeatherGateway } from 'src/promocode/application/ports/outcoming/httpGateway.interface';
import { OpenWeatherResponse } from 'src/promocode/business/dtos/dataMapper';
import * as openWeatherPayloadSample from '../../../../../../../testSettings/fixtures/open-weather-map.payload-sample.json';

export class HttpWeatherGatewaySpy implements IHttpWeatherGateway {
  protected _getWeatherByTownHaveBeenCalled = false;

  get getWeatherByTownHaveBeenCalled(): boolean {
    return this._getWeatherByTownHaveBeenCalled;
  }

  async getWeatherByTown(town: string): Promise<OpenWeatherResponse> {
    console.log('searching weather by town: ', town);
    this._getWeatherByTownHaveBeenCalled = true;

    return Promise.resolve({
      ...openWeatherPayloadSample,
    });
  }
}
