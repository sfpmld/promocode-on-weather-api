import { Inject } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import axios from 'axios';
import { OpenWeatherResponse } from '../../../../business/dtos/dataMapper';
import { IHttpWeatherGateway } from '../../../../application/ports/outcoming/httpGateway.interface';
import promocodeConfig from '../../../../config/app.config';

export class HttpWeatherGateway implements IHttpWeatherGateway {
  constructor(
    @Inject(promocodeConfig.KEY)
    private readonly config: ConfigType<typeof promocodeConfig>,
  ) {}
  async getWeatherByTown(town: string): Promise<OpenWeatherResponse> {
    const response = await axios.get(
      `${this.config.api.OPEN_WEATHER_API_URL}/?q=${town}&appid=${this.config.api.OPEN_WEATHER_API_KEY}`,
    );
    return response.data;
  }
}
