import { Avantage, PromocodeProps } from '../../../business/entities/promocode';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Promocode extends PromocodeProps {
  @PrimaryGeneratedColumn()
  _id: string;

  @Column()
  name: string;

  @Column('json', { nullable: true })
  avantage: Avantage;

  @Column('json', { nullable: true })
  restrictions: object[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
