import { IPromocodeRepository } from 'src/promocode/application/ports/outcoming/promocodeRepository.interface';
import { PromocodeProps } from 'src/promocode/business/entities/promocode';

import * as promocodeSample from '../../../../../../testSettings/fixtures/promocode-sample.json';

export class dbStorageSpy implements IPromocodeRepository {
  protected _spyData: PromocodeProps;
  private _calledWithPromocodeName: string;
  private _retrievePromocodeByNameWasCalled = false;

  setup(spyData: PromocodeProps) {
    this._spyData = spyData;
  }

  get retrievePromocodeByNameWasCalled(): boolean {
    return this._retrievePromocodeByNameWasCalled;
  }

  get beenCalledWithPromocodeName(): string {
    return this._calledWithPromocodeName;
  }

  async retrievePromocodeByName(
    promocodeName: string,
  ): Promise<PromocodeProps> {
    this._retrievePromocodeByNameWasCalled = true;
    this._calledWithPromocodeName = promocodeName;

    return {
      ...promocodeSample,
    };
  }

  async storePromocode(promocode: PromocodeProps): Promise<boolean> {
    console.log('stored promocode', promocode);
    return true;
  }
}
