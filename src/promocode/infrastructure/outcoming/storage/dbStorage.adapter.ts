import { InjectRepository } from '@nestjs/typeorm';
import { IPromocodeRepository } from 'src/promocode/application/ports/outcoming/promocodeRepository.interface';
import { Repository } from 'typeorm';
import {
  NullPromocode,
  PromocodeProps,
} from '../../../business/entities/promocode';
import { Promocode } from './promocode.model';

export class StorageAdapter implements IPromocodeRepository {
  constructor(
    @InjectRepository(Promocode) private repo: Repository<Promocode>,
  ) {}

  async retrievePromocodeByName(
    promocodeName: string,
  ): Promise<PromocodeProps> {
    const promocode = await this.repo.find({
      where: {
        name: promocodeName,
      },
      order: {
        createdAt: 'DESC',
      },
      take: 1,
    });

    return promocode ? promocode[0] : NullPromocode.create().toObject();
  }

  async storePromocode(promocode: PromocodeProps): Promise<boolean> {
    const isStored = await this.repo.save(promocode);
    return isStored ? true : false;
  }
}
