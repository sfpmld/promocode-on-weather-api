import { Body, Controller, Post } from '@nestjs/common';
import { AddPromocodeRequestDto } from '../../../../business/contracts/input/AddPromocodeRequest';
import { IAddPromocodeUseCase } from '../../../../application/ports/incoming/AddPromocodeCommand.interface';

@Controller('promocode')
export class AddPromocodeController {
  constructor(private readonly usecase: IAddPromocodeUseCase) {}

  @Post('/')
  async addPromocode(@Body() command: AddPromocodeRequestDto) {
    return this.usecase.handle(command);
  }
}
