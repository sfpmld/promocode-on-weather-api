import { Body, Controller, Get } from '@nestjs/common';
import { IGetPromotionUseCase } from '../../../../application/ports/incoming/GetPromotionRequest.interface';
import { GetPromotionRequestDto } from '../../../../business/contracts/input/getPromotionRequest';

@Controller('promocode')
export class GetPromocodeValidationController {
  constructor(private readonly usecase: IGetPromotionUseCase) {}

  @Get('/validation')
  async getPromocodeValidation(@Body() query: GetPromotionRequestDto) {
    return await this.usecase.handle(query);
  }
}
