import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../../../../../app.module';
import * as request from 'supertest';
import { AddPromocodeRequestDto } from '../../../../../business/contracts/input/AddPromocodeRequest';
import * as promocodeSample from '../../../../../../../testSettings/fixtures/promocode-sample.json';
import { DataSource, Repository } from 'typeorm';
import { Promocode } from '../../../../../../promocode/infrastructure/outcoming/storage/promocode.model';

// Helper
const getLastPromocodeByName = (repository) => async (name) =>
  await repository.find({
    where: {
      name: name,
    },
    order: {
      createdAt: 'DESC',
    },
    take: 1,
  });

// Test
let app: INestApplication;
let datasource: DataSource;
let promocodeRepository: Repository<Promocode>;
beforeAll(async () => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  app = moduleFixture.createNestApplication();
  await app.init();

  datasource = moduleFixture.get<DataSource>(DataSource);
  promocodeRepository = datasource.getRepository(Promocode);
});

afterAll(async () => {
  await promocodeRepository.clear();
  await app.close();
});

describe(`Story: Dedicated Users are able to add promocode entries`, () => {
  describe(`Rule: Users are able to add promocode thanks to route POST /promocode`, () => {
    describe(`Given incoming http request to add promocode`, () => {
      describe(`When handled by service`, () => {
        it(`Then it should succeed with status "${HttpStatus.CREATED}"`, async () => {
          // Arrange
          const expectedPromocodeName = 'WeatherCode';
          const incomingRequest: AddPromocodeRequestDto = {
            ...promocodeSample,
          };

          // Act
          return (
            request(app.getHttpServer())
              .post(`/promocode`)
              .send(incomingRequest)

              //Assert
              .expect(HttpStatus.CREATED)
              .then(async (res) => {
                const response = res.body;
                expect(response.promocode_name).toBe(expectedPromocodeName);

                const promocode = await getLastPromocodeByName(
                  promocodeRepository,
                )(expectedPromocodeName);

                expect(promocode[0]).toEqual(
                  expect.objectContaining({
                    name: expectedPromocodeName,
                  }),
                );
              })
          );
        });
      });
    });
  });
});
