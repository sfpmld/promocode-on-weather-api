import { HttpStatus, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../../../../../app.module';
import * as request from 'supertest';
import { AddPromocodeRequestDto } from '../../../../../business/contracts/input/AddPromocodeRequest';
import * as promocodeSample from '../../../../../../../testSettings/fixtures/promocode-sample.json';
import { DataSource, Repository } from 'typeorm';
import { Promocode } from '../../../../outcoming/storage/promocode.model';
import { GetPromotionRequestDto } from '../../../../../business/contracts/input/getPromotionRequest';
import { IDateProvider } from '../../../../../application/services/date-provider';
import { DateProviderSpy } from '../../../../../application/services/date-provider/mocks/dateProvider.spy';
import { PromocodeStatusEnum } from '../../../../../business/contracts/output/GetPromotionResponse';
import { IHttpWeatherGateway } from '../../../../../application/ports/outcoming/httpGateway.interface';
import { HttpWeatherGatewaySpy } from '../../../../outcoming/http-gateways/open-weather-map/mocks/httpWeatherGateway.spy';

// Test
let app: INestApplication;
let datasource: DataSource;
let promocodeRepository: Repository<Promocode>;
beforeAll(async () => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  })
    // Test special case to keep deternministic data
    .overrideProvider(IDateProvider)
    .useClass(DateProviderSpy)
    .overrideProvider(IHttpWeatherGateway)
    .useClass(HttpWeatherGatewaySpy)
    .compile();

  app = moduleFixture.createNestApplication();
  await app.init();

  datasource = moduleFixture.get<DataSource>(DataSource);
  promocodeRepository = datasource.getRepository(Promocode);
});

afterAll(async () => {
  await promocodeRepository.clear();
  await app.close();
});

describe(`Story: Users are able check their promocode validation`, () => {
  describe(`Rule: Users are able to validate or not their promocode by requesting the service GET /promocode/validation`, () => {
    describe(`Given incoming http request to check for a given promocode validation`, () => {
      describe(`When handled by service`, () => {
        it(`Then it should succeed with status "${HttpStatus.OK}"`, async () => {
          // Arrange
          const promocodeToAdd: AddPromocodeRequestDto = {
            ...promocodeSample,
            _id: null,
          };
          const alreadyCreated = await promocodeRepository.save(promocodeToAdd);
          const expectedPromocodeName = 'WeatherCode';
          const getPromotionRequest: GetPromotionRequestDto = {
            promocode_name: expectedPromocodeName,
            arguments: {
              age: 25,
              meteo: {
                town: 'Paris',
              },
            },
          };

          // Act
          return (
            request(app.getHttpServer())
              .get(`/promocode/validation`)
              .send({
                ...getPromotionRequest,
              })

              // Assert
              .expect(HttpStatus.OK)
              .then(async (res) => {
                const response = res.body;
                expect(response).toEqual(
                  expect.objectContaining({
                    promocode_name: alreadyCreated.name,
                    status: PromocodeStatusEnum.ACCEPTED,
                    avantage: {
                      percent: alreadyCreated.avantage.percent,
                    },
                  }),
                );
              })
          );
        });
      });
    });
  });
});
