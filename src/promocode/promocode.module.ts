import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IAddPromocodeUseCase } from './application/ports/incoming/AddPromocodeCommand.interface';
import { ICheckForPromocodeValidityUseCase } from './application/ports/incoming/CheckForPromocodeValidityRequest.interface';
import { IGetPromotionUseCase } from './application/ports/incoming/GetPromotionRequest.interface';
import { IHttpWeatherGateway } from './application/ports/outcoming/httpGateway.interface';
import { IPromocodeRepository } from './application/ports/outcoming/promocodeRepository.interface';
import {
  DateProvider,
  IDateProvider,
} from './application/services/date-provider';
import {
  CheckPromocodeValidityService,
  ICheckPromocodeValidityService,
} from './application/services/dsl-parser';
import {
  IRestrictionEvaluator,
  RestrictionEvaluator,
} from './application/services/dsl-parser/core/evaluateRestrictions';
import {
  IRecursiveDSLParser,
  RecursiveDSLParser,
} from './application/services/dsl-parser/core/recursiveDslParser';
import {
  Formatter,
  IFormatter,
} from './application/services/dsl-parser/core/stringFormatter';
import {
  ITemplateInterpoler,
  TemplateInterpoler,
} from './application/services/dsl-parser/core/templateInterpoler';
import { AddPromocodeCommandUseCase } from './application/use-cases/addPromocode.command';
import { CheckForPromocodeValidityUseCase } from './application/use-cases/checkForPromocodeValidity.request';
import { GetPromotionRequestUsecase } from './application/use-cases/getPromotion.request';
import { AddPromocodeController } from './infrastructure/incoming/nest/http/addPromocode.controller';
import { GetPromocodeValidationController } from './infrastructure/incoming/nest/http/getPromocodeValidation.controller';
import { HttpWeatherGateway } from './infrastructure/outcoming/http-gateways/open-weather-map';
import { StorageAdapter } from './infrastructure/outcoming/storage/dbStorage.adapter';
import { Promocode } from './infrastructure/outcoming/storage/promocode.model';
import promocodeConfig from './config/app.config';

@Module({
  imports: [
    ConfigModule.forFeature(promocodeConfig),
    HttpModule,
    TypeOrmModule.forFeature([Promocode]),
  ],
  controllers: [AddPromocodeController, GetPromocodeValidationController],
  providers: [
    // Use Cases
    {
      provide: IAddPromocodeUseCase,
      useFactory: (repository: IPromocodeRepository) =>
        new AddPromocodeCommandUseCase(repository),
      inject: [IPromocodeRepository],
    },
    {
      provide: IGetPromotionUseCase,
      useFactory: (
        repository: IPromocodeRepository,
        httpWeatherGateway: IHttpWeatherGateway,
        dateProvider: IDateProvider,
        validationUsecase: ICheckForPromocodeValidityUseCase,
      ) =>
        new GetPromotionRequestUsecase(
          repository,
          httpWeatherGateway,
          dateProvider,
          validationUsecase,
        ),
      inject: [
        IPromocodeRepository,
        IHttpWeatherGateway,
        IDateProvider,
        ICheckForPromocodeValidityUseCase,
      ],
    },
    {
      provide: ICheckForPromocodeValidityUseCase,
      useFactory: (validationService: ICheckPromocodeValidityService) =>
        new CheckForPromocodeValidityUseCase(validationService),
      inject: [ICheckPromocodeValidityService],
    },
    // DSL Services and related dependencies
    {
      provide: ICheckPromocodeValidityService,
      useFactory: (
        dslParser: IRecursiveDSLParser,
        templateInterpoler: ITemplateInterpoler,
        restrictionEvaluator: IRestrictionEvaluator,
      ) =>
        new CheckPromocodeValidityService(
          dslParser,
          templateInterpoler,
          restrictionEvaluator,
        ),
      inject: [IRecursiveDSLParser, ITemplateInterpoler, IRestrictionEvaluator],
    },
    {
      provide: IRecursiveDSLParser,
      useFactory: (formatter: IFormatter) => new RecursiveDSLParser(formatter),
      inject: [IFormatter],
    },
    {
      provide: ITemplateInterpoler,
      useFactory: (formatter: IFormatter) => new TemplateInterpoler(formatter),
      inject: [IFormatter],
    },
    {
      provide: IRestrictionEvaluator,
      useClass: RestrictionEvaluator,
    },
    {
      provide: IFormatter,
      useClass: Formatter,
    },
    // Infrastructure
    {
      provide: IPromocodeRepository,
      useClass: StorageAdapter,
    },
    {
      provide: IHttpWeatherGateway,
      useClass: HttpWeatherGateway,
    },
    {
      provide: IDateProvider,
      useClass: DateProvider,
    },
  ],
})
export class PromocodeModule {}
