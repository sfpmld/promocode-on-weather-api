import { IsNumber, IsString, ValidateNested } from 'class-validator';

export class Meteo {
  @IsString()
  is?: string;

  @IsNumber()
  temp?: number;
}

export class IsValidPromocodeRequestDto {
  @IsString()
  date?: string;

  @IsNumber()
  age?: number;

  @ValidateNested()
  meteo?: Meteo;
}
