import { IsObject, IsString, ValidateNested } from 'class-validator';
import { Avantage, PromocodeProps } from '../../entities/promocode';

export enum AddPromocodeStatusEnum {
  SUCCESS = 'success',
  FAILURE = 'failure',
}

export class AddPromocodeRequestDto implements PromocodeProps {
  @IsString()
  _id?: string;

  @IsString()
  name: string;

  @ValidateNested()
  avantage: Avantage;

  @IsObject()
  restrictions: object[];
}
