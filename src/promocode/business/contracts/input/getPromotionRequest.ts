import { IsNumber, IsString, ValidateNested } from 'class-validator';

export class Meteo {
  @IsString()
  town?: string;
}

export class Arguments {
  @IsNumber()
  age?: number;

  @ValidateNested()
  meteo?: Meteo;
}

export class GetPromotionRequestDto {
  @IsString()
  promocode_name: string;

  @ValidateNested()
  arguments: Arguments;
}
