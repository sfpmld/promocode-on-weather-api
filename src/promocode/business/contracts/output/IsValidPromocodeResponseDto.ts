import { Avantage } from '../../entities/promocode';

export class IsValidPromocodeResponseDto {
  promocode_name: string;
  status: string;
  avantage?: Avantage;
  reasons?: string[];
}
