import { Avantage } from '../../entities/promocode';

export enum PromocodeStatusEnum {
  ACCEPTED = 'accepted',
  DENIED = 'denied',
}

export class GetPromotionResponseDto {
  promocode_name: string;

  status: PromocodeStatusEnum | string;

  avantage?: Avantage;

  reason?: string;
}
