import { AddPromocodeStatusEnum } from '../input/AddPromocodeRequest';

export class AddPromocodeResponseDto {
  promocode_name: string;

  status: AddPromocodeStatusEnum;

  error?: Error | string;
}
