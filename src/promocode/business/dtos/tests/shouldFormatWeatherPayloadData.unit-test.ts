import * as samplePayload from '../../../../../testSettings/fixtures/open-weather-map.payload-sample.json';
import { DataMapper, MeteoDto, OpenWeatherResponse } from '../dataMapper';

class SUT {
  private testing: any;
  private result: MeteoDto;
  private input: OpenWeatherResponse;

  constructor(testing: typeof DataMapper) {
    this.testing = testing;
  }

  public processPayload(payload) {
    this.input = payload;
    this.result = this.testing.fromWeatherAPI(payload);
  }

  public shouldFormatWeatherPayloadData() {
    expect(this.result).toEqual(
      expect.objectContaining({
        is: this.input.weather[0].main.toLowerCase(),
        // temp in celsius
        temp: Math.floor(this.input.main.temp - 273.15),
      }),
    );
  }
}

let sut: SUT;
beforeEach(async () => {
  sut = new SUT(DataMapper);
});

describe(`Given received payload from openweather api`, () => {
  describe(`When processed by adapter`, () => {
    it(`Then it should format data according to exploitable data`, async () => {
      sut.processPayload(samplePayload);

      sut.shouldFormatWeatherPayloadData();
    });
  });
});
