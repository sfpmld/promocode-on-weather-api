import { IsNumber } from 'class-validator';
import { DataMapper } from '../dtos/dataMapper';
import { IsValidPromocodeResponseDto } from '../contracts/output/IsValidPromocodeResponseDto';

export class Avantage {
  @IsNumber()
  percent: number;
}

export class PromocodeProps {
  _id?: string;
  name: string;
  avantage: Avantage;
  restrictions: object[];
}

export class NullPromocode {
  private _id?: string;

  private name: string;

  private avantage: Avantage;

  private restrictions: object[];

  static create(): NullPromocode {
    return new NullPromocode();
  }

  private constructor() {
    this._id = '';
    this.name = '';
    this.avantage = { percent: 0 };
    this.restrictions = [];
  }

  public toObject(): PromocodeProps {
    return {
      _id: this._id,
      name: this.name,
      avantage: this.avantage,
      restrictions: this.restrictions,
    };
  }
}

export class Promocode {
  private _id: string;

  private name: string;

  private avantage: Avantage;

  private restrictions: object[];

  static create(
    _id: string,
    name: string,
    avantage: Avantage,
    restrictions: object[],
  ): Promocode {
    return new Promocode(_id, name, avantage, restrictions);
  }

  private constructor(
    _id: string,
    name: string,
    avantage: Avantage,
    restrictions: object[],
  ) {
    this._id = _id;
    this.name = name;
    this.avantage = avantage;
    this.restrictions = restrictions;
  }

  public getValidPromocodeResponse(): IsValidPromocodeResponseDto {
    return DataMapper.toViewSuccess(this.toObject());
  }

  public getDeniedPromocodeResponse(
    errors: string[],
  ): IsValidPromocodeResponseDto {
    return DataMapper.toViewFailure(this.toObject().name, errors);
  }

  public toObject(): PromocodeProps {
    return {
      _id: this._id,
      name: this.name,
      avantage: this.avantage,
      restrictions: this.restrictions,
    };
  }

  public toDatabase(): PromocodeProps {
    return DataMapper.toDatabase(this.toObject());
  }
}
