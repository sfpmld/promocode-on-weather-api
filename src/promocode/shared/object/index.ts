import { isEmpty, path } from 'ramda';
import { DataPath } from 'src/promocode/config/rosettaStone';

export type Constructor<T> = new (...args: any[]) => T;

export const isEmptyObject = (obj: any) => {
  return isEmpty(obj);
};

export const isStriclyObject = (value) => {
  return typeof value === 'object' && !Array.isArray(value);
};

export const getDataFromFieldsPath = (dataPath: DataPath, data: any) => {
  return path(dataPath, data);
};
