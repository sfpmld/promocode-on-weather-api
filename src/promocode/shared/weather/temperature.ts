export const convertKelvinToCelsius = (temp: number) => {
  return Math.floor(temp - 273.15);
};
