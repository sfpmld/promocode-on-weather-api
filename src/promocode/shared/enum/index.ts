export const isValueEnumKey = (value: any, enumType: any): boolean => {
  return Object.values(enumType).includes(value as unknown as typeof enumType);
};

export const retrieveEnumKeyFromValue = (value: any, enumType: any): string => {
  return Object.keys(enumType).find((key) => enumType[key] === value);
};
