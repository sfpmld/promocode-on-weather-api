export abstract class IUseCase<TRequest, TResponse> {
  abstract handle(param: TRequest): Promise<TResponse>;
}
