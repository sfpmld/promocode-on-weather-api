import { IsValidPromocodeRequestDto } from '../../src/promocode/business/contracts/input/isValidPromocodeRequest';

export const setupInterpolatedRestrictionsFixtures = (
  realData: IsValidPromocodeRequestDto,
) => {
  const { date, age, meteo } = realData;

  // from shouldInterpolateAnchorsInTemplates.unit-test.ts test inline snapshot
  return `(new Date('${date}') >= new Date('2019-01-01') && new Date('${date}') <= new Date('2020-06-30')   || captureErrors('Invalid date'))  && ((${age} === 40  || captureErrors('Invalid age'))  || ((${age} <= 30 && ${age} >= 15   || captureErrors('Invalid age'))  && ('${meteo.is}' === 'clear' && (${meteo.temp} >= 15 ) || captureErrors('Invalid meteo')) ))`;
};
