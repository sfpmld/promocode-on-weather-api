export const parsedRestrictions =
  "(@date >= new Date('2019-01-01') && @date <= new Date('2020-06-30')   || captureErrors('Invalid date'))  && ((@age === 40  || captureErrors('Invalid age'))  || ((@age <= 30 && @age >= 15   || captureErrors('Invalid age'))  && (@meteo.is === 'clear' && (@meteo.temp >= 15 ) || captureErrors('Invalid meteo')) ))";
