module.exports = {
  moduleFileExtensions: ['js', 'ts', 'json'],
  verbose: true,
  rootDir: '../../src',
  testRegex: '.unit-test.ts$|.int-test.ts|.spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  testEnvironment: 'node',
};
