module.exports = {
  moduleFileExtensions: ['js', 'ts', 'json'],
  verbose: true,
  rootDir: '../../src',
  testRegex: '.spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  testEnvironment: 'node',
};
