module.exports = {
  moduleFileExtensions: ['js', 'ts', 'json'],
  verbose: true,
  rootDir: '../../src',
  testRegex: '.int-test.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  testEnvironment: 'node',
};
